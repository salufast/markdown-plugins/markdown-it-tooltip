import test from 'tape'
import md from 'markdown-it'
import {tooltip} from '../dev/index.js'

function parse(text, options) {
  const parser = md().use(tooltip, options)
  return parser.render(text)
}

function customRenderer(tooltipText, tooltipNote) {
  return (
    '<tooltip><tooltip-text>' +
    tooltipText +
    '</tooltip-text><tooltip-note>' +
    tooltipNote +
    '</tooltip-note></tooltip>'
  )
}

test('markdown -> html (micromark)', (t) => {
  t.deepEqual(
    parse('A paragraph.'),
    '<p>A paragraph.</p>\n',
    'Basic markdown rendering should work'
  )
  t.deepEqual(
    parse('A paragraph.[text]{note}'),
    '<p>A paragraph.<span class="tooltip"><span class="tooltip-text">text</span><span class="tooltip-note">note</span></span></p>\n',
    'should support calls w notes'
  )
  t.deepEqual(
    parse('A paragraph.[text]{}'),
    '<p>A paragraph.[text]{}</p>\n',
    'should ignore calls w/o notes'
  )
  t.deepEqual(
    parse('A paragraph.[]{note}'),
    '<p>A paragraph.[]{note}</p>\n',
    'should ignore notes w/o calls'
  )
  t.deepEqual(
    parse('A paragraph.[te[xt]{note}'),
    '<p>A paragraph.[te<span class="tooltip"><span class="tooltip-text">xt</span><span class="tooltip-note">note</span></span></p>\n',
    'should not support l bracket in tooltip text'
  )
  t.deepEqual(
    parse('A paragraph.[te]xt]{note}'),
    '<p>A paragraph.[te]xt]{note}</p>\n',
    'should not support r bracket in tooltip text'
  )
  t.deepEqual(
    parse('A paragraph.[text]{no{te}'),
    '<p>A paragraph.[text]{no{te}</p>\n',
    'should not support l brace in tooltip text'
  )
  t.deepEqual(
    parse('A paragraph.[text]{no}te}'),
    '<p>A paragraph.<span class="tooltip"><span class="tooltip-text">text</span><span class="tooltip-note">no</span></span>te}</p>\n',
    'should not support r brace in tooltip text'
  )

  t.deepEqual(
    parse('A paragraph.[text]{note}', {renderTooltip: customRenderer}),
    '<p>A paragraph.<tooltip><tooltip-text>text</tooltip-text><tooltip-note>note</tooltip-note></tooltip></p>\n',
    'should parse with custom renderer function'
  )
  t.end()
})
